<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTagsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tags', function (Blueprint $table) {
            $table->bigIncrements('id')->index();
            $table->string('title');
            $table->timestamps();
        });

        Schema::create('guest_book_tag', function (Blueprint $table) {
            $table->integer('guest_book_id')->unsigned()->index();
//            $table->foreign('guest_book_id')->references('id')->on('guest_books')->onDelete('cascade');
            $table->integer('tag_id')->unsigned()->index();
//            $table->foreign('tag_id')->references('id')->on('tags')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tags');
        Schema::dropIfExists('guest_book_tag');
    }
}
