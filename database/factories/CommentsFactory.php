<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use \App\Models\GuestBook;
use Faker\Generator as Faker;

$factory->define(GuestBook::class, function (Faker $faker) {
    return [
        'author_name'=> $faker->name,
        'email'=> $faker->email,
        'homepage'=> $faker->url,
        'comment'=> $faker->realText(),

    ];
});
