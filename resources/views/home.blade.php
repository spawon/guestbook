@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Guest book</div>
                <div class="card-body">
                    @if(Auth::user())
                        <guest-book :user="{{Auth::user()}}"/>
                    @else
                        <guest-book/>
                    @endif

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
