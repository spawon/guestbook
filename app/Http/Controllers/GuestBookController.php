<?php

namespace App\Http\Controllers;

use App\Models\GuestBook;
use App\Models\Tag;
use Illuminate\Http\Request;

/**
 * Class GuestBookController
 * @package App\Http\Controllers
 */
class GuestBookController extends Controller
{
    /**
     * @param Request $request
     * @return mixed
     */
    public function get(Request $request){

        $comments = null;
    switch ($request->filter){
        case 'username':
            $comments = GuestBook::
            where('author_name', 'like', "%$request->data%")->
            skip($request->skip)->
            take(20)->
            with('tag')->
            orderBy('created_at', 'desc')->
            get();
            break;
        case 'email':
            $comments = GuestBook::
            where('email', 'like', "%$request->data%")->
            skip($request->skip)->
            take(20)->
            with('tag')->
            orderBy('created_at', 'desc')->
            get();
            break;
        case 'data':
            $comments = GuestBook::
            where('created_at', 'like', "%$request->data%")->
            skip($request->skip)->
            take(20)->
            with('tag')->
            orderBy('created_at', 'desc')->
            get();
            break;
        case 'tag':
            $tag = Tag::where('title', 'like', "%$request->data%")->first();
            if (!empty($tag))
                $comments = $tag->comment;
            else
                return 404;
            break;
        default:
            $comments = GuestBook::orderBy('created_at', 'desc')->skip($request->skip)->take(20)->with('tag')->get();

    }

        if ($comments->count() > 0)
            return $comments;
        else
            return 404;

    }

    /**
     * @param Request $request
     * @return int
     */
    public function post(Request $request){
        $err = false;
        if (iconv_strlen($request->name) < 3) {
            $err = true;
        }
        if (iconv_strlen($request->email) < 3) {
            $err = true;
        }
        if (iconv_strlen($request->comment) < 10) {
            $err = true;
        }
        if ($err)
            return $request;

         $guestBookInstanse = GuestBook::create([
                'author_name'=>$request->name,
                'email'=>$request->email,
                'homepage'=>'test',
                'comment'=>$request->comment,
            ]);

        if (isset($request->tags))
            foreach ($request->tags as $tag) {
                $tag = Tag::firstOrCreate(['title' => $tag]);
                $guestBookInstanse->tag()->attach($tag->id);
            }
         return 200;
    }

    /**
     * @param Request $request
     */
    public function update(Request $request){}

    /**
     * @param Request $request
     */
    public function delete(Request $request){}
}
